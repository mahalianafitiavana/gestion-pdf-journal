<?php
    require_once('../fpdf185/fpdf.php');
    require_once('../classes/CodeJ.php');
    require_once('../inc/connection.php');
    class PDFCompte extends FPDF    {
        protected $width = 0;
        function Header()  {
            $this->SetFont('Arial','BU',14);
            $this->Cell(0,10,'LES CODES JOURNAUX',0,1,'C'); 
            $this->Ln(10);          
        }
        function Footer()   {
            $this->SetY(-15);
            $this->SetFont('Arial','I',8);
            $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
        }
        function SetWidth($width) {
            $this->width = $width;
        }
        function Body($codes) {
            $this->SetWidth($this->GetPageWidth() - 40);
            $this->SetX(20);
            $this->SetFont('Arial','B',12);
            $this->Cell($this-> width/3,10,utf8_decode("Code"),1,0,'C');
            $this->Cell(2*$this->width/3,10,utf8_decode('Intitulé'),1,1,'C');
            
            $this->SetX(20);
            foreach ($codes as $code) {
                $this->CompteCell ($code);
            }
            
        }
        function CompteCell ($Code)  {
            $this->SetFont('Arial','',12);
            $this->Cell($this->width/3,10,$Code->getCode(),1,0,'C');
            $this->Cell(2*$this->width/3,10,utf8_decode($Code->getIntitule()),1,1,'C');
            $this->SetX(20);
        }    
    }
    
    
    $pdf = new PDFCompte("P","mm","A4");
    $pdf->AliasNbPages();
    $pdf->AddPage();
    $Comptes = CodeJ::getAll();
    //var_dump($Comptes)
    $pdf->Body($Comptes);
    // Add your PDF content here
    $pdf->Output();

?>