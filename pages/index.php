<?php
    session_start();
    if(isset($_SESSION["error"])){
        $error = $_SESSION['error'];
    }


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h4>Add new Tiers</h4>
    <form action="tiers.php" method="post">
        <label for="numero">Numero:</label><br>
        <input type="text" id="numero" name="numero"><br>
        <label for="intitule">Intitule:</label><br>
        <input type="text" id="intitule" name="intitule"><br>
        <input type="submit" value="Submit">
        <a href="tierspdf.php">Expoter en pdf</a>
    </form>
    <h4>Add new Code Journal</h4>
    <form action="CodeJ.php" method="post">
        <label for="code">Code:</label><br>
        <input type="text" id="code" name="code"><br>
        <label for="intitule">Intitule:</label><br>
        <input type="text" id="intitule" name="intitule"><br>
        <input type="submit" value="Submit">
        <a href="codeJpdf.php">Expoter en pdf</a>
    </form>
    <h4>Importer de nouveau Journal </h4>
    <form action="traitement.php" method="post" enctype="multipart/form-data" >
        <input type="hidden" name="class" value="journal">
        <input type="file" name="sheet" id="" accept=".csv">
        <input type="submit" value="import">
        <br>
        <a href="journalpdf.php">Expoter en pdf</a>
        <br>
    </form>

    <?php 
        if (isset($_GET['error']    )) { ?>
            <p style="color: red;"><?php echo $_GET['error']; ?> </p>            
    <?php } 
        if (isset($_GET['succes']    )) { ?>
            <p style="color: green;"><?php echo $_GET['succes']; ?> </p> 
    <?php }
        if(isset($error)) { 
            for ($i=0; $i < count($error) ; $i++) { 
                if ($error[$i] !== null) {
                    for ($j=0; $j < count($error[$i]) ; $j++) { 
                        echo "<p style='color:red;'>".$error[$i][$j]." à la ligne ".($i+2)."</p>";
                    }
                }
            } 
            unset($_SESSION['errors']); // Clear the errors after displaying them    
        }
    ?>
</body>
</html>