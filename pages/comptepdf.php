<?php
    require_once('../fpdf185/fpdf.php');
    require_once('../classes/Compte.php');
    require_once('../inc/connection.php');
    class PDFCompte extends FPDF    {
        protected $width = 0;
        function Header()  {
            $this->SetFont('Arial','BU',14);
            $this->Cell(0,10,'LISTE DES COMPTES',0,1,'C'); 
            $this->Ln(10);          
        }
        function Footer()   {
            $this->SetY(-15);
            $this->SetFont('Arial','I',8);
            $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
        }
        function SetWidth($width) {
            $this->width = $width;
        }
        function Body($Comptes) {
            $this->SetWidth($this->GetPageWidth() - 40);
            $this->SetX(20);
            $this->SetFont('Arial','B',12);
            $this->Cell($this-> width/3,10,utf8_decode("Numéro"),1,0,'C');
            $this->Cell(2*$this->width/3,10,utf8_decode('Intitulé du Compte'),1,1,'C');
            
            $this->SetX(20);
            foreach ($Comptes as $compte) {
                $this->CompteCell ($compte);
            }
            
        }
        function CompteCell ($compte)  {
            $this->SetFont('Arial','',12);
            $this->Cell($this->width/3,10,$compte->getCompte(),1,0,'C');
            $this->Cell(2*$this->width/3,10,utf8_decode($compte->getIntitule()),1,1,'L');
            $this->SetX(20);
        }    
    }
    
    
    $pdf = new PDFCompte("P","mm","A4");
    $pdf->AliasNbPages();
    $pdf->AddPage();
    $Comptes = Compte::getAll();
    //var_dump($Comptes)
    $pdf->Body($Comptes);
    // Add your PDF content here
    $pdf->Output();

?>