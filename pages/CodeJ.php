<?php
    require_once '../classes/CodeJ.php';

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $code = $_POST['code'];
        $intitule = $_POST['intitule'];
        $codeJ = new CodeJ($code, $intitule);
        if ($codeJ->exists() ) {
            Header("Location:index.php?error=Code Jornal with code $code already exists");
        } else {
            $codeJ->save();
            Header("Location:index.php?succes= Succesful Saving");
        }
    }
?>