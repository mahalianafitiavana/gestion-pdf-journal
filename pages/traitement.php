<?php
    session_start();
    include ("../inc/function.php");
    require_once("../classes/Journal.php");
    $file = $_FILES['sheet'];
    $class_name = $_POST['class'];
    if ($file['error'] == 0) {
        $fileType = pathinfo($file['name'], PATHINFO_EXTENSION);
        if ($fileType != 'csv') { // si le fichier n'est pas un fichier CSV
            header("Location:index.php?error=Le fichier importé n'est pas un fichier CSV");
        }
        else{
            $data = Journal::getDatafromCsv($file['tmp_name']);
            if ($data[0] instanceof Journal) {
                Journal::saveList($data);
                echo "Succes";
                header("Location:index.php?succes=Success! Fichier importé avec succès");
            }else{ // les exeption sont en string
                $_SESSION['error'] =  $data;
                header("Location:index.php");
            }
        }
    }
    else if ($file['error'] == 4) { // pas de fichier importer 
        header("Location:index.php?error=Veuillez selectionner un fichier");
    }
    else {
        header("Location:index.php?error=Erreur lors de l'importation du fichier");
    }


?>