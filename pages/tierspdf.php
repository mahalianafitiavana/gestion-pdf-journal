<?php
    require_once '../fpdf185/fpdf.php';
    require_once '../classes/Tiers.php';
    class PDF extends FPDF {
        function Header() {
            $this->SetFont('Arial','BU',14);
            $this->Cell(0,10,'LISTE DES TIERS',0,1,'C');
            $this->Ln(10);
        }
        function Footer() {
            $this->SetY(-15);
            $this->SetFont('Arial','I',8);
            $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
        }
        function Body($tiers) {
            $this->SetFont('Arial','B',12);
            $this->SetX(35);
            $this->Cell(15,10,'Type',1,0,'C');
            $this->Cell(60,10,'Numero',1,0,'C');
            $this->Cell(60,10,'Intutile',1,1,'C');
            $this->SetFont('Arial','',12);
            
            foreach ($tiers as $tier) {
                $this->tierCell($tier);
            }
        }
        function tierCell($tier){
            $this->SetX(35);
            $this->Cell(15,10,$tier->getType(),1,0,'C');
            $this->Cell(60,10,$tier->getNumero(),1,0,'C');
            $this->Cell(60,10,$tier->getIntitule(),1,1,'C');
        }
    }

    $pdf = new PDF("P","mm","A4");
    $pdf->AliasNbPages();
    $pdf->AddPage();
    $pdf->Body(Tiers::getAll());
    $pdf->Output();

?>