<?php
    require_once '../classes/Tiers.php';

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $numero = $_POST['numero'];
        $intitule = $_POST['intitule'];
        $tiers = new Tiers($numero, $intitule);
        if ($tiers->exists() ) {
            Header("Location:index.php?error=Tiers with numero $numero already exists");
        } else {
            $tiers->save();
            Header("Location:index.php?succes= Succesful Saving");
        }
    }
?>