<?php
    include_once ('../inc/connection.php');
    class Tiers {
        private $numero;
        private $intitule;
        private $type;
        public function getType() {
            return $this->type;
        }

        public function setType($type) {
            $this->type = $type;
        }
        public function getNumero() {
            return $this->numero;
        }

        public function setNumero($numero) {
            $this->numero = $numero;
        }

        public function getIntitule() {
            return $this->intitule;
        }

        public function setIntitule($intitule) {
            $this->intitule = $intitule;
        }
    
        public function __construct($numero = null, $intitule = null, $type = null) {
           if ($numero != null) $this->setNumero($numero);
           if ($intitule != null) $this->setIntitule($intitule);    
           if ($type != null) $this->setType($type);       
        }
    
        public function save() {
            try {
                $con = dbconnect();
                $query = "INSERT INTO tiers (type,numero, intitule) VALUES (?,?, ?)";
                $stmt = $con->prepare($query);
                $stmt->execute([$this->getType() ,$this->getNumero(), $this->getIntitule()]);
                $con = null;
            } catch (Exception $e) {
                throw $e;
            }
        }
        public static function getAll() {
            $query = "SELECT * FROM Tiers";
            try {
                $con = dbconnect();
                $stmt = $con->prepare($query);
                $stmt->execute();
                $tiers = [];
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $tiers[] = new Tiers($row['numero'], $row['intitule'], $row['type']);
                }
                return $tiers;
            } catch (Exception $e) {
                throw $e;
            }
        }
        
        public function exists() {            
            try {
                $con =  dbconnect();
                $stmt = $con->prepare('SELECT COUNT(*) FROM Tiers WHERE numero = :numero');
                $stmt->execute([':numero' =>$this ->getNumero()]);
                $count = $stmt->fetchColumn();
                return $count > 0;
                
            } catch (Exception $th) {
                throw $th;
            }
        }
    }



?>