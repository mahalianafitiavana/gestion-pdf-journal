<?php
    require_once("CodeJ.php");
    require_once("Tiers.php");
    require_once("Compte.php");
    class Journal {
        private $date;
        private $ref_piece;
        private $tiers;
        private $compte;
        private $libelle;        
        private $debit;
        private $credit;
        private $exceptions = array ();
        
        // Getters
        public function getDate() {
            return $this->date;
        }

        public function getRefPiece() {
            return $this->ref_piece;
        }

        public function getTiers() {
            return $this->tiers;
        }

        public function getCompte() {
            return $this->compte;
        }

        public function getLibelle() {
            return $this->libelle;
        }

        public function getDebit() {
            return $this->debit;
        }

        public function getCredit() {
            return $this->credit;
        }

        // Setters
        public function setDate($date) {
            $this->date = $date;
        }

        public function setTiers($t) {
            try {
                $tier =  new Tiers($t,"","") ;
                if(!$tier->exists()) {
                    throw new Exception("Le client ". $t ." n' existe pas" );
                }
                $this->tiers = $t;
            } catch (Exception $e) {
                $this->exceptions[] = $e->getMessage();
            }
        }

        public function setRefPiece($ref_piece) {
            // try {
            //     $code = new CodeJ($ref_piece,"") ;
            //     if (!$code->exists()) {
            //         throw new Exception("Le code Journal ". $ref_piece ." n' exite pas");
            //     }
                $this->ref_piece = $ref_piece;
            // } catch (Exception $e) {
            //     $this->exceptions[] = $e->getMessage();
            // }
        }
        
        public function setCompte($c) {
            try {
                $compte = new Compte($c,"");
                if(!$compte->exists()) {
                    throw new Exception("Le compte ". $c ." n' existe pas" );
                }
                $this->compte = $c;
            } catch (Exception $e) {
                $this->exceptions[] = $e->getMessage();
            }
        }
        
        public function setLibelle($libelle) {
            $this->libelle = $libelle;
        }
        
        public function setDebit($debit) {
            if($debit === null){
                $this->debit = 0;
            }
            if (is_numeric($debit)) {
                $this->debit = $debit;
            }else{
                $debit = str_replace(" ","", $debit);  #eleminer les espaces
                $debit = str_replace(",",".", $debit);
                $debit = doubleval( $debit );
                $this->debit = $debit;
            }
        }
          
        public function setCredit($credit) {
            if ($credit === null) {
                $this->credit = 0;
            }
            if (is_numeric( $credit )) {        
                $this->credit = $credit; 
            }else{
                $credit = str_replace(" ","", $credit);
                $credit = str_replace(",",".", $credit );
                $credit = doubleval( $credit );
                $this->credit = $credit;
            }
        }
        public function setExceptions($exceptions) {
            $this->exceptions = $exceptions;
        }
        
        public function getExceptions() {
            return $this->exceptions;
        }
        public function __construct($date = null, $ref_piece = null,$compte = null, $tiers = null , $libelle = null, $debit = null, $credit = null) {
            if($date != null) $this->setDate($date);
            if($ref_piece != null) $this->setRefPiece($ref_piece);
            if($tiers != null ) $this->setTiers($tiers);
            if($compte != null) $this->setCompte($compte);
            if($libelle != null) $this->setLibelle($libelle);
            if($debit != null) $this->setDebit($debit);
            if($credit != null) $this->setCredit($credit);
            if (count($this->getExceptions()) === 0 ) {
                $this->exceptions = null;
            }
        }
        public static function getAll() {
            $query = "SELECT * FROM journal";
            try {
                $con = dbconnect();
                $stmt = $con->prepare($query);
                $stmt->execute();
                $journaux = [];
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $journaux[] = new Journal($row['date'], $row['ref_piece'], $row['compte'], 
                    $row['tiers'],$row['libelle'],$row['debit'],$row['credit']);
                }
                return $journaux;
            } catch (Exception $e) {
                throw $e;
            }
        }
        public static function getDatafromCsv($file_name){
            $file = fopen($file_name, 'r+');
            $exception = array(); $list = array();
            if ($file === false) {
                throw new Exception ("Error while opening $file_name ");
            }else {
                fgetcsv($file);  // sauter les nom des columns
                while (($row = fgetcsv($file,255,';')) !== false) {
                    $journal =  new Journal($row[0],$row[1],$row[2],$row[3],$row[5],$row[6],$row[7]); 
                    $list[] = $journal;
                    $exception [] = $journal->getExceptions();
                }                
            }
            if (!empty(array_filter($exception))) { // verifier si il y a des exceptions not null
                return $exception;
            }else{
                return $list;
            }
        }
        function save($con) {
            $query =  "insert into journal ( date, ref_piece,compte,tiers,libelle,debit,credit) values (?,?,?,?,?,?,?)";
            try {
                $stmt = $con->prepare($query);
                $stmt->execute([$this->getDate () ,$this->getRefPiece(), $this->getCompte(), 
                $this->getTiers(),$this->getLibelle(),$this->getDebit(),$this->getCredit()]);
                return;
            } catch (Exception $e) {
                throw $e;
            }
        }
    
        static function saveList($clientArray) {
            try {
                $con = dbconnect();
                foreach ($clientArray as $client) {
                    $client->save($con);
                }
                $con = null;
                return;
            } catch (Exception $e) {
                throw $e;
            }
        }
    }
    ?>