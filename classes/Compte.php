<?php
    // include($_SERVER['DOCUMENT_ROOT'] . '/S4/gestion-pdf-journal/inc/connection.php');
    include_once('../inc/connection.php');
    class Compte {
        private $compte;
        private $intitule;

        public function getCompte()    {
            return $this->compte;
        }

        public function setCompte($compte)    {
            $this->compte = $compte;
        }

        public function getIntitule()    {
            return $this->intitule;
        }

        public function setIntitule($intitule)    {
            $this->intitule = $intitule;
        }
        public function __construct($compte = null, $intitule = null) {
            if ($compte !== null )  $this->setCompte($compte);
            if ( $intitule !== null)   $this->setIntitule($intitule);
        }
        public function exists() {
            try {
                $con = dbconnect();
                $query = "select count(*) as count from compte where compte = ?";
                $stmt = $con->prepare($query);
                $stmt->execute([$this->getCompte()]);
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                $con = null;
                return $row['count'] > 0;
            } catch (Exception $e) {
                throw $e;
            }
        }
        static function getAll() {
            try {
                $con = dbconnect();
                $query = "select * from compte";
                $stmt = $con->prepare($query);
                $stmt->execute();
                $comptes = array();
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {                    
                    $comptes[] = new Compte($row['compte'], $row['intitule']);
                }
                $con = null;
                return $comptes;
            } catch (Exception $e) {
                throw $e;
            }
        }
        function save ($con ) {
            $query =  "insert into compte (compte, intitule) values (?,? )";
            try {
                $stmt = $con->prepare ( $query );                
                $intituleValue = iconv('UTF-8', 'UTF-8//IGNORE', $this->getIntitule());
                print($intituleValue );
                $stmt->execute([ intval($this->getCompte()),$intituleValue ]);
                return;
            } catch (Exception $e) {
                throw $e;
            }
        }
        static function saveList ($comptaArray) {
            try {
                $con = dbconnect();
                foreach ($comptaArray as $compte) {
                    $compte->save($con);
                }
                $con = null;
                return;
            } catch (Exception $e) {
                throw $e;
            }
        }
       
        
    }    
?>