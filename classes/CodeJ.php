<?php
    include_once ('../inc/connection.php');
    class CodeJ {
        private $code;
        private $intitule;
    
        
        public function getCode() {
            return $this->code;
        }
        
        public function setCode($code) {
            $this->code = $code;
        }
        
        public function getIntitule() {
            return $this->intitule;
        }
        
        public function setIntitule($intitule) {
            $this->intitule = $intitule;
        }
        
        public function __construct($code = null, $intitule = null) {
            if ($code != null) $this->setCode($code);
            if ($intitule != null) $this->setIntitule($intitule);
        }
        public function save() {
            try {
                $conn = dbconnect();
                $stmt = $conn->prepare("INSERT INTO CodeJ (code, intitule) VALUES (:code, :intitule)");
                $stmt->execute([':code' => $this->code, ':intitule' => $this->intitule]);
            } catch (\Throwable $th) {
                throw $th;
            }
        }
    
        public function exists() {
            try {
                $conn = dbconnect();
                $stmt = $conn->prepare("SELECT COUNT(*) FROM CodeJ WHERE code = :code");
                $stmt->bindParam(':code', $this->code);
                $stmt->execute();
                $count = $stmt->fetchColumn();
                return $count > 0;
            } catch (\Throwable $th) {
                throw $th;
            }
        }
        public static function getAll() {
            try {
                $conn = dbconnect();
                $stmt = $conn->prepare("SELECT * FROM CodeJ");
                $stmt->execute();
                $list = array();
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $list[] = new CodeJ($row['code'], $row['intitule']);
                }
                return $list;
            } catch (\Throwable $th) {
                throw $th;
            }
        }
    }
?>