<?php
   include_once ('../inc/connection.php');
    class Client {
        public $id;
        public $nom;
        public $prenom;
        public $adresse;

        public function getId()    {
            return $this->id;
        }

        public function setId($id)     {
            $this->id = $id;
        }

        public function getNom()        {
            return $this->nom;
        }

        public function setNom($nom)        {
            $this->nom = $nom;
        }

        public function getPrenom()   {
            return $this->prenom;
        }

        public function setPrenom($prenom)  {
            $this->prenom = $prenom;
        }

        public function getAdresse()  {
            return $this->adresse;
        }

        public function setAdresse($adresse)   {
            $this->adresse = $adresse;
        }

        public function __construct($id = null, $nom = null, $prenom = null, $adresse = null) {
            if ($id !== null && $nom !== null && $prenom !== null && $adresse !== null) {
                $this->setId($id);
                $this->setNom($nom);
                $this->setPrenom($prenom);
                $this->setAdresse($adresse);
            }
        }
        public static function getAll() {
            $query = "SELECT * FROM client";
            try {
                $con = dbconnect();
                $stmt = $con->prepare($query);
                $stmt->execute();
                $clients = [];
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $clients[] = new Client($row['id'], $row['nom'], $row['prenom'], $row['adresse']);
                }
                return $clients;
            } catch (Exception $e) {
                throw $e;
            }
        }

        function save($con) {
            $query =  "insert into client (id, nom, prenom , adresse) values (?, ?,?,?)";
            try {
                $stmt = $con->prepare($query);
                $nom = iconv('UTF-8', 'UTF-8//IGNORE', $this->getNom());
                $prenom = iconv('UTF-8', 'UTF-8//IGNORE', $this->getPrenom());
                $adresse = iconv('UTF-8', 'UTF-8//IGNORE', $this->getAdresse());
                $stmt->execute([$this->getId() ,$nom, $prenom, $adresse]);
                return;
            } catch (Exception $e) {
                throw $e;
            }
        }
    
        static function saveList($clientArray) {
            try {
                $con = dbconnect();
                foreach ($clientArray as $client) {
                    $client->save($con);
                }
                $con = null;
                return;
            } catch (Exception $e) {
                throw $e;
            }
        }

    }

?>