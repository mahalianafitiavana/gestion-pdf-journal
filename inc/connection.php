<?php 
    function dbconnect () {
        try {        
            $PARAM_hote = "localhost";
            $PARAM_nom_bd = "gestion";
            $PARAM_utilisate = "postgres";
            $PARAM_mot_passe = "postgres";
            $connexion = new PDO("pgsql:host=$PARAM_hote;dbname=$PARAM_nom_bd", $PARAM_utilisate, $PARAM_mot_passe );
            return $connexion;            
        }
        catch(Exception $e) {        
            echo 'Erreur : '.$e->getMessage().'<br />';        
            echo 'N° : '.$e->getCode(); 
            die();
        } 
    }
?>