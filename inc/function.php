<?php
    include_once ('connection.php');
    require ('../classes/Journal.php');
    function getTableInfo($table) {
        $columns = array();
        try {
            $con = dbconnect();
            $statement = $con->prepare("SELECT * FROM " . $table . " LIMIT 0");   
            $statement->execute();
            $columnCount = $statement->columnCount();
            for ($i = 0; $i < $columnCount; $i++) {
                $meta = $statement->getColumnMeta($i);
                $columns[] = array('name' => $meta['name'], 'type' => $meta['native_type']);
            }
            $con = null;
            return $columns;
        } catch (Exception $e) {
            echo 'Error: ' . $e->getMessage() . '<br />';
            die();
        }
    }
    function getDatefromCsv($file_name,$class_name){
        $file = fopen($file_name, 'r+');
        $list = array();
        $exception = array();
        if ($file === false) {
            throw new Exception ("Error while opening $file_name ");
        }else {
            fgetcsv($file);
            try {
                while (($row = fgetcsv($file,255,';')) !== false) {
                    if ($class_name == "journal") {// build compte object
                        $journal =  new Journal($row[0],$row[1],$row[2],$row[3],$row[5],$row[6],$row[7]); 
                        $list[] = $journal;
                        $exception [] = $journal->getExceptions();
                    }
                }
            } catch (\Exception $th) {
                $exception[] =  $th->getMessage();
            }
        }
        if (empty( $exception)) {
            echo "no excption ";
        }else {
            var_dump($exception);
        }
        return $list;
    }

?>